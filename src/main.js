import { createApp } from 'vue'
import App from './App.vue'

import PrimeVue from 'primevue/config'
import Card from 'primevue/card';
import Timeline from 'primevue/timeline';

import 'primevue/resources/themes/saga-blue/theme.css'       //theme
import 'primevue/resources/primevue.min.css'                 //core css
import 'primeicons/primeicons.css'                           //iconsconst app=createApp(App);

const app=createApp(App);
// const plane=createApp(Planes)

// plane.use(PrimeVue)
app.use(PrimeVue);

app.component('Card',Card);
app.component('Timeline',Timeline)
// plane.component('Card',Card)

app.mount('#app')

